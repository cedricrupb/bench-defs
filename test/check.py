#!/usr/bin/env python3
import sys
import os
from urllib import request
from lxml import etree

BENCHMARK_DIRECTORY = "../sv-benchmarks/c"


class DTDResolver(etree.Resolver):
    def __init__(self):
        self.cache = dict()

    def resolve(self, url, d_id, context):
        if url.startswith("http"):
            if url in self.cache:
                dtd_content = self.cache[url]
            else:
                dtd_content = self._download(url)
                self.cache[url] = dtd_content
            return self.resolve_string(dtd_content, context)
        return super().resolve(url, d_id, context)

    def _download(self, url):
        with request.urlopen(url) as inp:
            dtd_content = inp.read()
        return dtd_content


PARSER = etree.XMLParser(dtd_validation=True)
"""XML parser used in unit tests."""
PARSER.resolvers.add(DTDResolver())


def _check_valid(xml_file):
    try:
        etree.parse(xml_file, PARSER)
        return []
    except etree.ParseError as e:
        return [e]
    except etree.XMLSyntaxError as e:
        return [e]


def _get_tasks(xml_file):
    xml_root = etree.parse(xml_file)
    return xml_root.findall("tasks")


def _check_task_defs_match_set(xml_file):
    errors = []
    for task_tag in _get_tasks(xml_file):
        name = task_tag.get("name")
        if not name:
            errors.append(
                "Task tag is missing name in line {}".format(task_tag.sourceline)
            )

        includes = task_tag.findall("includesfile")
        if len(includes) != 1:
            errors.append(
                "Expected exactly one <includesfile> tag for tasks {}".format(name)
            )
        else:
            include = includes[0]
            included_set = include.text
            benchmark_dir = "/".join(included_set.split("/")[:-1])
            if not benchmark_dir.startswith(BENCHMARK_DIRECTORY):
                errors.append(
                    "Expected benchmark directory to be {} for tasks {}".format(
                        BENCHMARK_DIRECTORY, name
                    )
                )
            set_file = included_set.split("/")[-1]
            if not set_file.endswith(".set"):
                errors.append("Set name does not end on '.set': {}".format(set_file))

            set_name = ".".join(set_file.split(".")[:-1])
            if not set_name == name:
                errors.append(
                    "Set name not consistent with tasks name: {} vs. {}".format(
                        set_name, name
                    )
                )

    return errors


def _check_same_tasks_in_xmls(benchmark_defs):
    tasks_to_defs = dict()
    for bench_def in benchmark_defs:
        for task in (t.get("name") for t in _get_tasks(bench_def)):
            if task not in tasks_to_defs:
                tasks_to_defs[task] = set()
            tasks_to_defs[task].add(bench_def)
    expected_def_count = len(benchmark_defs)
    errors = list()
    for task, defs in tasks_to_defs.items():
        if len(defs) != expected_def_count:
            missing_defs = set(benchmark_defs) - defs
            if len(missing_defs) < len(defs):
                errors.append(
                    "MISSING: {} missing in {}".format(task, ", ".join(missing_defs))
                )
            else:
                errors.append("UNEXPECTED: {} only in {}".format(task, ", ".join(defs)))
    return errors


def _check_xmls_in(directory):
    success = True
    benchmark_defs = [
        os.path.join(directory, x) for x in os.listdir(directory) if x.endswith(".xml")
    ]
    for xml in benchmark_defs:
        errors = _check_valid(xml)
        errors += _check_task_defs_match_set(xml)
        if not errors:
            print("SUCCESS:", xml)
        else:
            print("ERROR:", xml)
            for msg in errors:
                print(msg)
        success = success and not errors
    errors = _check_same_tasks_in_xmls(benchmark_defs)
    success = success and not errors
    print("\n".join(errors))
    return success


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: {} <benchmark_directory>".format(sys.argv[0]))
        sys.exit(1)
    directory = sys.argv[1]
    if not os.path.exists(directory):
        print("Directory {} doesn't exist".format(directory))
    success = _check_xmls_in(directory)
    sys.exit(0 if success else 1)
